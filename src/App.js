import {useState, useEffect} from 'react';
import Card from './components/Card'
import Styled from 'styled-components';
import {MemoryGameContext} from './contexts/MemoryGameContext';
import {CardsData} from './lib/cardsData'

// set up some styled components
const MemoryGameHeader = Styled.div`
	text-align: center;
	font-size: 2rem;
	font-weight: bold;
`
const CardsContainer = Styled.div`
	display:flex;
	flex-wrap: wrap;
	justify-content: space-evenly;
	width: 960px;
	margin: 0 auto;
`;

const WinnerContainer = Styled.div`
	margin: 0 auto;
	text-align: center;
	margin-top: 30%;
`

// sord the card data for layout
let cards = CardsData.sort((a,b)=> (0.5 - Math.random()));

function App() {
	// set up some states
	const [flippedCards, setflippedCards] = useState([]);
	const [matchedPairs, setMatchedPairs] = useState([]);

	// when the cards flip we need to:
	useEffect(()=>{
		// check if we have two cards to compare
		if(flippedCards.length === 2 ){
			// compare the values, strip the leters from the ID's
			let flippedCardA = flippedCards[0].replace(/[A-B]/g, '');
			let flippedCardB = flippedCards[1].replace(/[A-B]/g, '');
			if(flippedCardA === flippedCardB){
				// if they match, we add them to the array of matched pairs
				setTimeout(() => {
					setMatchedPairs([...matchedPairs, ...flippedCards]);
					setflippedCards([]);
				}, 300);
			} else {
				// if the don't match we clear the flipped cards array, causing them to flip back over
				setTimeout(() => {
					setflippedCards([]);
				}, 300);
			}
		}
	},[flippedCards]); // only run the useEffect if the flippedCards state changes
	return (
		<div className="App">
			<MemoryGameContext.Provider value={{flippedCards, setflippedCards}}>
				<MemoryGameHeader>Memory Game</MemoryGameHeader>
				{matchedPairs.length !== cards.length ?
					<CardsContainer>
						{cards.map((card)=> (
							<Card key={card.id} {...card} matchedPairs={matchedPairs}/>
						))}
					</CardsContainer>
				:
					<WinnerContainer>
						<h2> YOU WIN! </h2>
						<button onClick={() => (setMatchedPairs([]))}>Start Over</button>
					</WinnerContainer>
				}
			</MemoryGameContext.Provider>
		</div>
	);
}

export default App;
