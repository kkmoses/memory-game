import React,{useState,useContext,useEffect} from 'react'
import Styled from 'styled-components'
import {MemoryGameContext} from '../contexts/MemoryGameContext'

const StyledCard = Styled.div`
	background-color: transparent;
	width: 125px;
	height: 175px;
	border: 1px solid #f1f1f1;
	margin: 10px;
	cursor:pointer;

	&.selected .card-inner,
	&.matched .card-inner {
		transform: rotateY(180deg);
	}
	&.matched {
		box-shadow: 0px 0px 5px green;
		animation: 1s fadeOut;
		animation-delay: 0.3s;
  		animation-fill-mode: forwards;
	}
	@keyframes fadeOut {
		0% {
			opacity: 1;
		}
		100% {
			opacity: 0;
			visibility: hidden;
		}
	}
	.card-inner {
		position: relative;
		
		width: 100%;
		height: 100%;
		text-align: center;
		transition: transform 0.3s;
		transform-style: preserve-3d;
	}

	.front, .back {
		border-radius: 5px;
		padding: 15px;
		box-sizing: border-box;
		font-size: 22px;
		position: absolute;
		text-align: center;
		width: 100%;
		height: 100%;
		-webkit-backface-visibility: hidden; /* Safari */
		backface-visibility: hidden;

		p {
			margin-top: 50%;
		}
	}

	.front {
		background-color: #bbb;
		color: black;
		transform: rotateY(180deg);
		font-size: 42px;
		font-weight: bold;
	}

	.back {
		background-color: dodgerblue;
		color: white;
	}
`;


export default function Card({id, value, matchedPairs}) {
	// grab the context from the App
	const {flippedCards, setflippedCards} = useContext(MemoryGameContext);

	// when we flip a card
	const flipCard = () => {
		// make sure we only flip 2 at a time
		if (flippedCards.length <= 1) {
			// add the current card's ID to the flippCards state
			setflippedCards([...flippedCards, id]);
		}
	}

	const unFlipCard = () => {
		// removing the current card from the state
		setflippedCards(flippedCards.filter((card)=> card !== id));
	}
	
	return (
		<StyledCard className={`card-container ${flippedCards.includes(id) ? "selected": ""} ${matchedPairs.includes(id) === true ? "matched": ""}`}>
			<div className={`card-inner`}>
				<div className="back" onClick={() => flipCard()}>
					<p>Rate.Com</p>
				</div>
				<div className="front" onClick={() => unFlipCard()}>
					<p>{value}</p>
				</div>
			</div>
		</StyledCard>
	)
}
